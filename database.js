/*
    Database Interacting Functions
        - For insertion functions, 0 indicates success, anything else indicates failure
*/

const SQLite = require('better-sqlite3');
const sql = new SQLite('projectDatabase.sqlite');

function getProject(ref) {
    return sql.prepare(`SELECT * FROM projects WHERE reference = ?;`).get(ref);
}

function getContribution(project, user) {
    return sql.prepare(`SELECT * FROM projectUserLink WHERE projectID = ? AND userID = ?`).all(project.id, user.id);
}

function getWebhook(channelId) {
    return sql.prepare(`SELECT * FROM webhookSettings WHERE channelID = ?;`).get(channelId);
}

function getBirthday(user) {
    return sql.prepare("SELECT * FROM birthdays WHERE user = ?").get(user.id);
}



module.exports = {
    //Check and Create Tables
    setupTables() {
        // Loads the project table
        let projectTable = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'projects';").get();
        if (!projectTable['count(*)']) 
        {
            // If the table isn't there, create it and setup the database correctly.
            sql.prepare("CREATE TABLE projects (id INTEGER PRIMARY KEY, reference TEXT, projectname TEXT, description TEXT, todo TEXT, image BLOB);").run();
            // Ensure that the "id" row is always unique and indexed.
            sql.prepare("CREATE UNIQUE INDEX index_projects_id ON projects (id);").run();
            sql.pragma("synchronous = 1");
            sql.pragma("journal_mode = wal");
        }
        
        // Loads the user table
        let userTable = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'users';").get();
        if (!userTable['count(*)']) 
        {
            sql.prepare("CREATE TABLE users (id INTEGER PRIMARY KEY, user TEXT, subtitle TEXT, profileinfo TEXT);").run();
            sql.prepare("CREATE UNIQUE INDEX index_users_id ON users (id);").run();
        }

        // Loads the link table
        let projectUserLink = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'projectUserLink';").get();
        if (!projectUserLink['count(*)']) 
        {
            sql.prepare("CREATE TABLE projectUserLink (id INTEGER PRIMARY KEY, projectID INTEGER, userID INTEGER, note TEXT);").run();
            sql.prepare("CREATE UNIQUE INDEX index_projectUserLink_id ON projectUserLink (id);").run();
        }

        // Loads the webhook settings table
        let webhookSettings = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'webhookSettings';").get();
        if (!webhookSettings['count(*)']) 
        {
            sql.prepare("CREATE TABLE webhookSettings (id INTEGER PRIMARY KEY, channelID TEXT, name TEXT, image TEXT);").run();
            sql.prepare("CREATE UNIQUE INDEX index_webhookSettings_id ON webhookSettings (id);").run();
        }

        // Loads the birthday table
        let birthdayTable = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'birthdays';").get();
        if (!birthdayTable['count(*)']) 
        {
            sql.prepare("CREATE TABLE birthdays (id INTEGER PRIMARY KEY, user TEXT, month INTEGER, day INTEGER, ticked INTEGER);").run();
            sql.prepare("CREATE UNIQUE INDEX index_birthdays_id ON birthdays (id);").run();
        }
    },
    
    //User Functions
    getUser(userId) {
        return sql.prepare(`SELECT * FROM users WHERE user = ?;`).get(userId);
    },

    insertUser(userId, subtitle = "A Valuable Contributor", profInfo = "A member of the coolest team around.") {
        let stmt = sql.prepare(`INSERT INTO users (user, subtitle, profileinfo) VALUES (?, ?, ?);`);
        stmt.run(userId, subtitle.substring(0,50), profInfo.substring(0,200));
        return 0;
    },

    updateUser(userId, field, update, limit = 999) {
        sql.prepare(`UPDATE users SET ${field} = ? WHERE user = ?;`).run(update.substring(limit), userId);
    },

    deleteUser(userId) {
        sql.prepare(`DELETE FROM users WHERE user = ${userId};`).run();
        sql.prepare(`DELETE FROM projectUserLink WHERE userid = ${userId};`).run();
    },


    //Project Functions
    getProject,

    getProjectWithId(projectId) {
        return sql.prepare(`SELECT * FROM projects WHERE id = ${project.projectID}`).get(projectId);
    },

    getAllProjects() {
        return sql.prepare(`SELECT * FROM projects`).all();
    },

    insertProject(ref, name = "New Project", desc = "No description.", todo = "Nothing yet!") {
        let check = getProject(ref);
        if(!check) {
            let stmt = sql.prepare(`INSERT INTO projects (reference, projectname, description, todo) VALUES (?, ?, ?, ?);`);
            stmt.run(ref.substring(0,20), name.substring(0,50), desc.substring(0,200), todo);
            return 0;
        }
        else {
            return -1;
        }

    },

    updateProject(ref, field, update, limit = 999) {
        sql.prepare(`UPDATE projects SET ${field} = ? WHERE reference = ?;`).run(update.substring(limit), ref);
    },

    deleteProject(ref, projectId) {
        sql.prepare(`DELETE FROM projects WHERE reference = ?;`).run(ref);
        sql.prepare(`DELETE FROM projectUserLink WHERE projectid = ?;`).run(projectId);
    },

    wipeProjects() {
        sql.prepare(`DELETE FROM projects;`).run();
    },


    //Collective Functions
    getProjectsOfUser(userId) {
        return sql.prepare(`SELECT * FROM projectUserLink WHERE userID = ?`).all(userId);
    },

    getUsersOfProject(projectId) {
        return sql.prepare(`SELECT * FROM projectUserLink WHERE projectID = ?`).all(projectId);
    },


    //Contribution Functions
    getContribution,

    insertContribution(project, user, note = "General Purpose Contributor") {
        let check = getContribution(project, user);
        if(!check) {
            let stmt = sql.prepare(`INSERT INTO projectUserLink (projectID, userID, note) VALUES (?, ?, ?);`);
            stmt.run(project.id, user.id, note.substring(0,100));
            return 0;
        }
        else {
            return -1;
        }
    },

    updateContribution(project, user, note = "General Purpose Contributor") {
        sql.prepare(`UPDATE projectUserLink SET note = ? WHERE projectID = ? AND userID = ?;`).run(note, project.id, user.id);
    },

    deleteContribution(project, user) {
        sql.prepare(`DELETE FROM projectUserLink WHERE projectID = ? AND userID = ?`).run(project.id, user.id);
    },


    //Webhook Functions
    getWebhook,

    getAllWebhooks() {
        return sql.prepare("SELECT * FROM webhookSettings").all();
    },

    insertWebhook(channelId, name, img) {
        let check = getWebhook(channelId);
        if(!check) {
            
            let stmt = sql.prepare(`INSERT INTO webhookSettings (channelID, name, image) VALUES (?, ?, ?);`);
            stmt.run(channelId, name, img);
            return 0;
        }
        else {
            return -1;
        }
    },

    updateWebhookPic(channelId, picURL) {
        sql.prepare(`UPDATE webhookSettings SET image = ? WHERE channelID = ?`).run(picURL, channelId);
    },

    deleteWebhook(channelId) {
        sql.prepare(`DELETE FROM webhookSettings WHERE channelID = ?;`).run(channelId);
    },


    //Birthday functions
    getBirthday,

    getAllBirthdays() {
        return sql.prepare("SELECT * FROM birthdays").all();
    },

    setBirthday(user, month, day) {
        let check = getBirthday(user);
        if(!check) {
            let stmt = sql.prepare(`INSERT INTO birthdays (user, month, day, ticked) VALUES (?, ?, ?, 0);`);
            stmt.run(user.id, month, day);
        }
        else {
            let stmt = sql.prepare(`UPDATE birthdays SET month = ?, day = ? WHERE user = ?`);
            stmt.run(month, day, user.id);
        }
        return 0;
    },

    deleteBirthday(user) {
        sql.prepare(`DELETE FROM birthdays WHERE user = ?;`).run(user.id);
    },

    setTicked(userId, ticked) { //0 = false, 1 = true
        let t = 0;
        if (ticked) {
            t = 1;
        }
        sql.prepare(`UPDATE birthdays SET ticked = ? WHERE user = ?`).run(t, userId);
    }

};