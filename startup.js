const Discord = require("discord.js");

const db = require("./database.js");
const fs = require('fs');
const {google} = require('googleapis');
const youtube = google.youtube('v3');
const {LAST_TIME_CALLED} = require('./timeStorage.json');

function makeCalls(client)
{
    client.webhook.settings.forEach(async (msgSettings) =>
    {
        try
        {
            let call = await youtube.activities.list({ //API Request
                        key: process.env.YOUTUBE_API_KEY,
                        part: ["snippet", "contentDetails"],
                        channelId: msgSettings.channelID,
                        maxResults: 50,
                        publishedAfter: client.webhook.lastCall
                        });
            
            call.data.items.forEach((item) =>
            {
                
                if(item.snippet.type == 'upload') 
                {
                    console.log(item);
                    client.webhook.webClient.send(webhookPost(item), {username: msgSettings.name, avatarURL: msgSettings.image});
                }  
            });
        }
        catch(e)
        {
            console.error(e);
            console.log("API Request failed");
        }
    });
    client.webhook.lastCall = new Date().toISOString();
    writeTime(client.webhook.lastCall);
}

function webhookPost(item)
{
    msg = `Attention to All:\n**${item.snippet.channelTitle}** uploaded **${item.snippet.title}** at ${convertDate(item.snippet.publishedAt)}\nhttps://www.youtube.com/watch?v=${item.contentDetails.upload.videoId}`;
    return msg;
}

function convertDate(date)
{
    dateObj = new Date(Date.parse(date));
    return dateObj.toLocaleString('en-US', { timeZone: 'EST' });
}

function writeTime(lastCall)
{
    fs.writeFile('./timeStorage.json', `{\n\t"LAST_TIME_CALLED": "${lastCall}"\n}`, () => console.log());
}


function startup(client)
{
    db.setupTables();
    client.webhook = { 
        webClient: new Discord.WebhookClient(process.env.WEBHOOK_ID, process.env.WEBHOOK_TOKEN, [contentType = "application/json"]),
        settings: db.getAllWebhooks(),
        lastCall: LAST_TIME_CALLED
    };
    makeCalls(client);
    console.log("Webhook Initialization Successful");
    setInterval(makeCalls, 300000, client);

    client.welcomeChannel = client.serverRef.channels.resolve(process.env.WELCOME_CHANNEL_ID);
    client.announcementsChannel = client.serverRef.channels.resolve(process.env.ANNOUNCEMENT_CHANNEL_ID);
}

module.exports = {startup};