const fs = require("fs");
const db = require("./database.js");
const roleIDs = require("./roleids.json");
//A set of general functions for all commands to use

async function getServerMember(client, memberID)
{
    let member = await client.serverRef.members.fetch(memberID);
    //console.log(member);
    return member;
}

async function isAdmin(message, user = message.author)
{
    let member = await getServerMember(message.client, user.id);
    return member.hasPermission('ADMINISTRATOR') || member.roles.highest.id == roleIDs.admin;
}

function checkForReferenceWord(message, args, func)
{
    if(!Array.isArray(args) || !args.length)
    {
        message.channel.send("You need to specify a reference word.");
    }
    else
    {
        let ref = args.shift().toLowerCase();
        func(message, ref, args);
    }
}

async function grabDM(user = message.author)
{
    let userDM = user.dmChannel;
    if(!userDM)
    {
        try
        {
            userDM = await user.createDM();
        }
        catch(e)
        {
            console.error(e);
        }
    }
    return userDM;
}

async function sendDM(user = message.author, info = "This is a DM.")
{
    try
    {
        let userDM = await grabDM(user);
        userDM.send(info);
    }
    catch(e)
    {
        console.error(e);
    }
}

function setCommands(client)
{
    //Folders where commands are located (Basic/Project/User/Webhook)
    let categories = fs.readdirSync(process.env.COMMAND_PATHWAY, {withFileTypes: true});

    for(const category of categories)
    {
        //Load all commands from a category folder
        if(category.isDirectory())
        {
            let typePath = fs.readdirSync(process.env.COMMAND_PATHWAY + `/${category.name}`);
            for(const file of typePath)
            {
                const command = require(process.env.COMMAND_PATHWAY + `/${category.name}/${file}`);
                client.commands.set(command.name, command);
            }
        }
    }
}


function groundedPermissions() {
    return {
        ADMINISTRATOR: false,
        CREATE_INSTANT_INVITE: false,
        KICK_MEMBERS: false,
        BAN_MEMBERS: false,
        MANAGE_CHANNELS: false,
        MANAGE_GUILD: false,
        ADD_REACTIONS: false,
        VIEW_AUDIT_LOG: false,
        PRIORITY_SPEAKER: false,
        STREAM: false,
        //VIEW_CHANNEL: false,
        SEND_MESSAGES: false,
        SEND_TTS_MESSAGES: false,
        MANAGE_MESSAGES: false,
        EMBED_LINKS: false,
        ATTACH_FILES: false,
        READ_MESSAGE_HISTORY: false,
        MENTION_EVERYONE: false,
        USE_EXTERNAL_EMOJIS: false,
        VIEW_GUILD_INSIGHTS: false,
        CONNECT: false,
        SPEAK: false,
        MUTE_MEMBERS: false,
        DEAFEN_MEMBERS: false,
        MOVE_MEMBERS: false,
        USE_VAD: false,
        CHANGE_NICKNAME: false,
        MANAGE_NICKNAMES: false,
        MANAGE_ROLES: false,
        MANAGE_WEBHOOKS: false,
        MANAGE_EMOJIS: false
      };
}

/*
function groundedPermissionsText() {
    return {
        MANAGE_CHANNELS: false,
        //MANAGE_ROLES: false,
        MANAGE_WEBHOOKS: false,
        CREATE_INSTANT_INVITE: false,
        SEND_MESSAGES: false,
        EMBED_LINKS: false,
        ATTACH_FILES: false,
        ADD_REACTIONS: false,
        USE_EXTERNAL_EMOJIS: false,
        MENTION_EVERYONE: false,
        MANAGE_MESSAGES: false,
        READ_MESSAGE_HISTORY: false,
        SEND_TTS_MESSAGES: false
    }
}

function groundedPermissionsVoice() {
    return {
        CREATE_INSTANT_INVITE: false,
        CONNECT: false,
        SPEAK: false,
        STREAM: false,
        USE_VAD: false,
        PRIORITY_SPEAKER: false,
        MUTE_MEMBERS: false,
        DEAFEN_MEMBERS: false,
        MOVE_MEMBERS: false
    }
}
*/

function checkBirthdays(client) {
    let currDate = new Date();
    let month = currDate.getMonth() + 1; //Get current month +1 (Jan = 1, Dec = 12)
    let day = currDate.getDate(); //Get current day
    let year = currDate.getFullYear(); //Get current year
    let bdays = db.getAllBirthdays(); //Get all birthdays in database

    let wishing = []; //List of all people to wish
    let wishString = "";

    bdays.forEach(bday =>
    {
        if(bday.ticked == 0) {
            if(bday.month == month && bday.day == day) {
                wishing.push(bday.user);
            }
            else { //February 29th exception, people with birthdays on the 29th of February will be wished on the 28th instead for non-leap years
                if(bday.month == month && month == 2 && bday.day == 29) {
                    let leapyear = (year % 100 != 0) && (year % 4 == 0);
                    if(!leapyear && day == 28) {
                        wishing.push(bday.user);
                    } 
                }
            }
        }

        if( (bday.month != month || bday.day != day) && bday.ticked == 1) { //Remove "tick" if not birthday
            db.setTicked(bday.user, false);
        }
    });

    let wishCount = 0;
    let wishedSize = wishing.length;

    wishing.forEach(wished => 
    {
        if(wishCount == wishedSize - 1 && wishedSize != 1) {
            wishString += "and ";
        }
        wishString += ( "<@" + wished + "> ");
        wishCount++;
    });

    try {
        if(wishString != "") {
            wishString = "Everybody wish a happy birthday to " + wishString;
            client.announcementsChannel.send(wishString);
        }
        wishing.forEach(wished => 
        {
            db.setTicked(wished, true);
        });
    }
    catch(e) {
        console.error(e);
    }
}

module.exports = {getServerMember, isAdmin, checkForReferenceWord, grabDM, sendDM, setCommands, 
    groundedPermissions, checkBirthdays};