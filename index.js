const Discord = require("discord.js");
const fs = require("fs");
const nodeSchedule = require("node-schedule");

require("dotenv").config();



//const pm2 = require('pm2');
const db = require("./database.js");
const {startup} = require("./startup.js");
const {isAdmin, setCommands, checkBirthdays} = require("./basicFunctions");

const client = new Discord.Client();

client.commands = new Discord.Collection(); //Collection of commands
client.inSetup = [];

const welcomeMsgs = ["This is a welcome message"];
//Fill this array out with custom messages for the bot to use when a person joins the server

setCommands(client); //Require all the commands



client.on('ready', async () =>
{
    client.serverRef = await client.guilds.fetch(process.env.SERVER_ID); //The server the bot runs on, used to refer to the server by passing around the client
    startup(client);
    console.log('Project Fun Bot on');
    checkBirthdays(client); //Do birthday stuff after startup complete
    nodeSchedule.scheduleJob('0 0 * * *', function(){
        checkBirthdays(client);
    }) //Run checkBirthdays at 12 am (midnight) everyday
});

//Implements a command handler. The command is used as a key, while the function is the value.
client.on('message', message =>
{
    //Code for dealing with commands
    if(message.author.bot || !message.content.startsWith(process.env.PREFIX) || client.inSetup.indexOf(message.author.id) != -1) return;

    const args = message.content.slice(process.env.PREFIX.length).trim().split(' '); //Gets all arguments
    const command = args.shift().toLowerCase(); //Seperates the command from the rest of the arguments

    while(args.indexOf('') != -1) //Removes all excess spaces from the args array (args[x] == '')
    {
        args.splice(args.indexOf(''), 1);
    }
    console.log(args);
    
    if (!client.commands.has(command)) //If the command isn't found in the commands collection
    {
        message.channel.send("Apologies, this command cannot be found.");
        return;
    }

    try //Tries to execute the command
    {
        let cmd = client.commands.get(command);
        if(cmd.admin && !isAdmin(message))
        {
            message.channel.send("Only admins can use that command.");
        }
        else
        {
            cmd.execute(message, args);
        }
    } 
    catch(e) //If the command fails to execute for whatever reason
    {
        console.error(e);
        message.channel.send("Command failed to execute. Tell WBot he did something stupid.");
        if(client.commands.get(command).execute == undefined) //Runs if there is no execute function in the command's module
        {
            message.channel.send("This command doesn't have a function to execute.");
        }
    }

});


client.on('guildMemberAdd', member => 
{
    let idx = Math.floor(Math.random() * welcomeMsgs.length)
    let msg = `Welcome to the server **${member.user}**. ` + welcomeMsgs[idx];
    client.welcomeChannel.send(msg);
});


client.on('guildMemberRemove', member =>
{
    db.deleteUser(member.id);
    db.deleteBirthday(member.id);
});

/*
client.on('error', e =>
{
    if(e instanceof FetchError) {
        console.log("Client could not connect to Discord's servers");
        return 1;
    }
    else {
        console.log("An error has occured");
    }
});*/

client.login(process.env.BOT_TOKEN); //Logs the bot into Discord