function displayWebhook(message)
{
    let info = "";
    message.client.webhook.settings.forEach(setting => 
    {
        info += `__${setting.name}__\n${setting.channelID}\n`;
    });
    if(info == "") {
        info = "No webhooks currently registered.";
    }
    message.channel.send(info);    
}

module.exports = 
{
    name: 'displaywebhook',
    description: 'Shows channel IDs and display names of all channels "registered "to the video link sharing webhook.',
    arguments: 'None',
    type: 'Webhook',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        displayWebhook(message);
    }
};