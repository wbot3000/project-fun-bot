const db = require("../../database.js");

function addWebhook(message, args)
{
    if(message.client.webhook.length >= 30)
    {
        message.channel.send("Maximum number of channels added.");
    }
    else
    {
        if(!Array.isArray(args) || !args.length)
        {
            message.channel.send("You need to specify a Youtube Channel ID in order to add a channel.");
        }
        else
        {
            let channelId = args.shift();
            let check = db.getWebhook(channelId);
            if(!check)
            {
                let newSettings = {
                    channelID: channelId,
                    name: (args.length >= 1) ? args.join(" ") : "A Cool Webhook",
                    image: ""
                };
                message.client.webhook.settings.push(newSettings);
                db.insertWebhook(channelId, newSettings.name, newSettings.image);
                message.channel.send("Channel successfully registered.");
            }
            else
            {
                message.channel.send("This channel is already registered.");
            }
        }
    }
}

module.exports = 
{
    name: 'addwebhook',
    description: 'Adds a channel to the video link sharing webhook.',
    arguments: '<Youtube Channel ID> [Name]',
    type: 'Webhook',
    admin: true,
    hidden: false,

    execute(message, args)
    {
        addWebhook(message, args);
    }
};