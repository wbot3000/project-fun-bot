const db = require("../../database.js");

function removeWebhook(message, args)
{
    if(!Array.isArray(args) || !args.length)
    {
        message.channel.send("You need to specify a Youtube Channel ID in order to remove a channel.");
    }
    else
    {
        let channelId = args[0];
        let check = db.getWebhook(channelId);
        if(!check)
        {
            message.channel.send("This channel isn't registered with the webhook.");
        }
        else
        {
            db.deleteWebhook(channelId);
            message.client.webhook.settings = db.getAllWebhooks();
            message.channel.send("This channel has been removed.");
        }
    }
}

module.exports = 
{
    name: 'removewebhook',
    description: 'Removes a channel from the video link sharing webhook.',
    arguments: '<Youtube Channel ID>',
    type: 'Webhook',
    admin: true,
    hidden: false,

    execute(message, args)
    {
        removeWebhook(message, args);
    }
};