const db = require("../../database.js");

function webhookPicture(message, args)
{
    if(!Array.isArray(args) || !args.length)
    {
        message.channel.send("You need to specify a Youtube Channel ID in order to change its associated picture.");
    }
    else if(args.length == 1)
    {
        message.channel.send("You need to specify an image URL.");
    }
    else
    {
        let channelId = args[0];
        let check = db.getWebhook(channelId);
        if(!check)
        {
            message.channel.send("This channel isn't registered with the webhook.");
        }
        else
        {
            pictureURL = args[1];
            db.updateWebhookPic(channelId, pictureURL);
            message.channel.send("Picture updated.");
        }
    }
}

module.exports = 
{
    name: 'webhookpic',
    description: 'Adds a picture corresponding to a channel detected by the video link sharing webhook.',
    arguments: '<Youtube Channel ID> <Image URL>',
    type: 'Webhook',
    admin: true,
    hidden: false,

    execute(message, args)
    {
        webhookPicture(message, args);
    }
};