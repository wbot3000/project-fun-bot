const db = require("../../database.js");
const {checkBirthdays} = require("../../basicFunctions.js");

const months = ["January", "February", "March", "April", "May", "June", "July", "August",
"September", "October", "November", "December"];

const dayCounts = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];


function setBirthday(message, args)
{
    let bdayUser = message.mentions.users.first();
    if(bdayUser == undefined)
    {
        bdayUser = message.author;
    }
    let month = parseInt(args[0]);
    if(Number.isNaN(month) || month < 1 || month > 12) {
        message.channel.send(`${args[0]} is not a valid month number (1-12).`);
    }
    else {
        let day = parseInt(args[1]);
        if(Number.isNaN(day)  || day < 1 || day > dayCounts[month-1]) {
            message.channel.send(`${args[1]} is not a valid day number for the month of ${months[month-1]} (1-${dayCounts[month-1]}).`);
        }
        else {
            db.setBirthday(bdayUser, month, day);
            message.channel.send(`Birthday for ${bdayUser} set for ${months[month-1]} ${day}.`);
            checkBirthdays(message.client);
        }
    }
}

module.exports = {
    name: 'setbirthday',
    description: 'Set a date for the birthday of a user. If nobody is mentioned, the user will be assumed to be you.',
    arguments: '<Month> <Day> (User)',
    type: 'Birthday',
    admin: true,
    hidden: false,
    
    execute(message, args)
    {
        setBirthday(message, args);
    }
};