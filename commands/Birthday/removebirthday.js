const db = require("../../database.js");

function removeBirthday(message)
{
    let removedBdays = message.mentions.users;

    removedBdays.forEach(user => 
    {
        db.deleteBirthday(user);
    });

    if(removedBdays.size == 0) {
        db.deleteBirthday(message.author);
        message.channel.send("Your birthday has been unregistered with the bot.");
    }
    else {
        message.channel.send("All mentioned birthdays have been unregistered with the bot.");
    }
}

module.exports = 
{
    name: 'removebirthday',
    description: 'Removes birthday records from the bot. Not mentioning anyone will remove your own record.',
    arguments: '(Users)',
    type: 'Birthday',
    admin: true,
    hidden: false,

    execute(message, args)
    {
        removeBirthday(message);
    }
};