const db = require("../../database.js");

const {checkForReferenceWord} = require("../../basicFunctions");

function leaveProject(message, ref, args)
{
    let project = db.getProject(ref);

    if(!project)
    {
        message.channel.send("A project with that reference word does not exist.");
    }
    else
    {
        let user = message.author.id;
        let check = db.getContribution(project, user);
        if(!check)
        {
            message.channel.send("You're not contributing to this project, so you can't be removed from it.");
        }
        else
        {
            db.deleteContribution(project, user);
            message.channel.send("You have been removed from this project.");
        }
    }
}

module.exports = {
    name: 'leaveproject',
    description: `Lets you remove yourself from a project's contributor list`,
    arguments: '<Reference Word>',
    type: 'Project',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        checkForReferenceWord(message, args, leaveProject);
    }
};