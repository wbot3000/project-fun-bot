const db = require("../../database.js");


const phases = {
  REFERENCE: 1,
  NAME: 2,
  DESC: 3
}

function createNewProject(message, args)
{
    message.client.inSetup.push(message.author.id);
    let reference = null;
    let projectname = "N/A";
    let description = "N/A";

    message.channel.send("First, select what reference word you want to give your project. This word will be used in commands relating to your project." +
    " If you put in multiple words, only the first one will be used as the reference. Max length stored is 20 characters.");
    let collector = message.channel.createMessageCollector(m => {return m.author.id == message.author.id;}); //Collects responses
    let state = phases.REFERENCE; //Determines what field to collect

    collector.on("collect", (m) =>
    {
      if(state == phases.REFERENCE) //For collecting the reference word
      {
        //let stmt = sql.prepare(`SELECT reference FROM projects WHERE reference = ?;`)
        if(m.content.indexOf(" ") == -1)
        {
          reference = m.content
        }
        else
        {
          reference = m.content.slice(0, m.content.indexOf(" "));
        }

        if(!reference) //Checks if something was written;
        {
          message.channel.send("You need to specify a reference word.");
        }
        else
        {
          if(!db.getProject(reference)) //Checks if the reference word is already in use
          {
            state = phases.NAME; //Switches state to collecting project name
            message.channel.send("Now write the project's name. Max length stored is 50 characters.");
          }
          else
          {
            message.channel.send("Reference word is already in use. Please specify another one");
          }
        }
      }
      else if(state == phases.NAME) //For collecting the project's name
      {
        if(m.content != "")
        {
          projectname = m.content;
        }
        state = phases.DESC; //Switches state to collecting description
        message.channel.send("Finally, write a description for the project. Max length stored is 200 characters.");
      }
      else //For collecting the project's description
      {
        if(m.content != "")
        {
          description = m.content;
        }
        db.insertProject(reference, projectname, description);
        message.channel.send("Project created!");
        collector.stop();
        message.client.inSetup.splice(message.client.inSetup.indexOf(message.author.id), 1);
      }
    });
}

module.exports = {
    name: 'createproject',
    description: 'Lets you create a new project for the bot to keep track of.',
    arguments: 'None (has a setup process)',
    type: 'Project',
    admin: true,
    hidden: false,
    
    execute(message, args)
    {
        createNewProject(message);
    }
};