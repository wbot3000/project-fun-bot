const Discord = require('discord.js');
const db = require("../../database.js");

const {checkForReferenceWord} = require("../../basicFunctions");

function displayContributors(message, ref, args)
{
    let proj = sql.prepare(`SELECT * FROM projects WHERE reference = ?`);
    proj = proj.get(ref);
    if(!proj)
    {
        message.channel.send("A project with that reference word does not exist.");
    }
    else
    {
        let contributorList = sql.prepare(`SELECT userID, note FROM projectUserLink WHERE projectID = ${proj.id}`).all();
        if(!Array.isArray(contributorList) || !contributorList.length)
        {
            message.channel.send(`${proj.projectname} currently has no contributors.`);
        }
        else
        {
            let infoEmbed = new Discord.MessageEmbed()
            .setColor([200, 200, 200])
            .setTitle(`Contributors for ${proj.projectname}:`);
            //let infoString = `__Contributors for ${proj.projectname}:__`;
            contributorList.forEach(async (entry) =>
            {
                let name = message.client.users.cache.get(entry.userID).username;
                infoEmbed.addField(name, entry.note);
                // if(infoString + `\n${name}: ${entry.note}`.length > 2000)
                // {
                //     message.channel.send(infoString);
                //     infoString = `\n${name}: ${entry.note}`;
                // }
                // else
                // {
                //     infoString += `\n${name}: ${entry.note}`;
                // }
            });
            message.channel.send({embed: infoEmbed});
        }
    }
}

module.exports = {
    name: "contributors",
    description: "Displays a list of contributors that are working on a project",
    arguments: "<Reference Word>",
    type: "Project",
    admin: false,
    hidden: false,

    execute(message, args)
    {
        checkForReferenceWord(message, args, displayContributors);
    }
};