const db = require("../../database.js");;

const {checkForReferenceWord} = require("../../basicFunctions");

function updateTask(message, ref, args)
{
    if(args == null)
    {
        note = "General Purpose Contributor";
    }
    else
    {
        note = args.join(" ");
    }

    let user = message.author.id;
    let project = db.getProject(ref);

    if(!project)
    {
        message.channel.send("A project with that reference word does not exist.");
    }
    else
    {
        let check = db.getContribution(project, user);
        if(!check)
        {
            db.insertContribution(project, user, note);
            message.channel.send(`You weren't a member of ${project.projectname} before, but you've been added with your provided information.`);
        }
        else
        {
            db.updateContribution(project, user, note);
            message.channel.send("You're contribution information has been changed.");
        }
    }
}

module.exports = {
    name: 'updatecontribution',
    description: 'Edit your contribution description for projects you are in.',
    arguments: '<Reference Word> [New Info]',
    type: 'Project',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        checkForReferenceWord(message, args, updateTask);
    }
}