const db = require("../../database.js");

const {checkForReferenceWord} = require("../../basicFunctions");

function deleteProject(message, ref)
{
    let projToDelete = db.getProject(ref);
    if(!projToDelete)
    {
      message.channel.send("A project with that reference word does not exist.");
    }
    else
    {
      db.deleteProject(ref, projToDelete.id)
      message.channel.send("The project associated with that reference word has been deleted");
    }
}

module.exports = 
{
    name: 'deleteproject',
    description: 'Deletes a project from the bot.',
    arguments: '<Reference Word>',
    type: 'Project',
    admin: true,
    hidden: false,

    execute(message, args)
    {
        checkForReferenceWord(message, args, deleteProject);
    }
};