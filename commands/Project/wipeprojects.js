const db = require("../../database.js");

async function wipeProjects(message)
{
    try
    {
      let newMsg = await message.channel.send(`Are you sure you want to wipe the entire project list? (Type "yes" minus quotes to wipe, type anything else to not wipe).`);

      const filter = (response) =>
      {
        return response.author.id === message.author.id;
      };

      const collector = newMsg.channel.createMessageCollector(filter);

      collector.on('collect', async (response) =>
      {
          try
          {
            if(response.content.toLowerCase() == "yes")
            {
              db.wipeProjects();
              message.channel.send("Projects successfully wiped.");
            }
            else
            {
              message.channel.send("Projects not wiped.");
            }
            collector.stop();      
          }
          catch(e)
          {
            console.error(e);
          }
      });
    }
    catch(e)
    {
      console.error(e);
    }
}

module.exports = {
    name: 'wipeprojects',
    description: 'Removes all projects from the bot.',
    arguments: 'None',
    type: 'Project',
    admin: true,
    hidden: false,

    execute(message, args)
    {
        wipeProjects(message);
    }
};