const db = require("../../database.js");

const {checkForReferenceWord} = require("../../basicFunctions");

function joinProject(message, ref, args)
{
    if(args == null)
    {
        note = "General Purpose Contributor";
    }
    else
    {
        note = args.join(" ");
    }

    let user = message.author.id;
    let project = db.getProject(ref);

    if(!project)
    {
        message.channel.send("A project with that reference word does not exist.");
    }
    else
    {
        let check = db.getContribution(project, user);
        if(!check)
        {
            db.insertContribution(project, user);
            message.channel.send(`You've joined ${project.projectname}.`);
        }
        else
        {
            message.channel.send("You're already contributing to this project.");
        }
    }
}

module.exports = {
    name: 'joinproject',
    description: 'Lets you list yourself as a contributor to a project',
    arguments: '<Reference Word>, [Note] (Max length stored is 100 characters.)',
    type: 'Project',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        checkForReferenceWord(message, args, joinProject);
    }
};