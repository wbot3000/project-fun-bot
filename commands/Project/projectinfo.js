const Discord = require('discord.js');
const db = require("../../database.js");

const {checkForReferenceWord} = require("../../basicFunctions");

function displayInfo(message, ref)
{
    let proj = db.getProject(ref);
    if(!proj)
    {
      message.channel.send("Project cannot be found.");
    }
    else
    {
      let projEmbed = new Discord.MessageEmbed()
      .setColor([200, 200, 200])
      .setTitle(proj.projectname)
      .setDescription(proj.description)
      .addField('TODO:', proj.todo);
      message.channel.send({embed: projEmbed});
    }
}


module.exports = {
    name: 'projectinfo',
    description: 'Displays the information of a project.',
    arguments: '<Reference Word>',
    type: 'Project',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        checkForReferenceWord(message, args, displayInfo);
    }    
};