//TODO: Work on Grounded Permissions

const db = require("../../database.js");

const {checkForReferenceWord, groundedPermissions} = require("../../basicFunctions.js");
const roleIDs = require("../../roleids.json");

const titles = ["🔔project-head-announcements", "📮suggestion-box", "📄script-drafts", "🔈voice-lines", "🎶music"]

async function channelize(message, ref, args)
{
    let proj = db.getProject(ref);
    if(!proj)
    {
        message.channel.send("A project with that reference word does not exist.");
    }
    else
    {
        let manager = message.client.serverRef.channels;
        let category = await manager.create(proj.projectname, {type: "category"});
        let newChannel;
        //let groundedRole = message.client.serverRef.roles.cache.get(roleIDs.grounded);

        /*
        await category.updateOverwrite(groundedRole, {
            ADMINISTRATOR: false,
            CREATE_INSTANT_INVITE: false,
            KICK_MEMBERS: false,
            BAN_MEMBERS: false,
            MANAGE_CHANNELS: false,
            MANAGE_GUILD: false,
            ADD_REACTIONS: false,
            VIEW_AUDIT_LOG: false,
            PRIORITY_SPEAKER: false,
            STREAM: false,
            //VIEW_CHANNEL: false,
            SEND_MESSAGES: false,
            SEND_TTS_MESSAGES: false,
            MANAGE_MESSAGES: false,
            EMBED_LINKS: false,
            ATTACH_FILES: false,
            READ_MESSAGE_HISTORY: false,
            MENTION_EVERYONE: false,
            USE_EXTERNAL_EMOJIS: false,
            VIEW_GUILD_INSIGHTS: false,
            CONNECT: false,
            SPEAK: false,
            MUTE_MEMBERS: false,
            DEAFEN_MEMBERS: false,
            MOVE_MEMBERS: false,
            USE_VAD: false,
            CHANGE_NICKNAME: false,
            MANAGE_NICKNAMES: false,
            MANAGE_ROLES: false,
            MANAGE_WEBHOOKS: false,
            MANAGE_EMOJIS: false
          });*/

        let numTitles = titles.length;
        for(i = 0; i < numTitles; i++) {
            newChannel = await manager.create(titles[i], {type: "text", topic: proj.projectname});
            await newChannel.setParent(category);
            //await newChannel.updateOverwrite(groundedRole, groundedPermissions());
        }
        newChannel = await manager.create(`${proj.projectname} Chat`, {type: "voice"});
        await newChannel.setParent(category);
        //await newChannel.updateOverwrite(groundedRole, groundedPermissions());
    }
}

module.exports = {
    name: 'channelize',
    description: 'Sets up a group of channels for a project.',
    arguments: '<Reference Word>',
    type: 'Project',
    admin: true,
    hidden: false,
    
    execute(message, args)
    {
        checkForReferenceWord(message, args, channelize);
    }
};