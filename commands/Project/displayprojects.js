const Discord = require('discord.js');
const db = require("../../database.js");

function displayProjects(message)
{
  let projects = db.getAllProjects();
  if(!Array.isArray(projects) || !projects.length)
  {
    message.channel.send("No projects at the moment.");
  }
  else
  {
    let projEmbed = new Discord.MessageEmbed()
    .setColor([200, 200, 200])
    .setTitle('Project List');
    for(i = 0; i < projects.length; i++)
    {
      projEmbed.addField(projects[i].projectname, projects[i].reference);
    }
    message.channel.send({embed: projEmbed});
  }
}

function displayProjectsExpanded(message)
{
  let projects = db.getAllProjects();
  if(!Array.isArray(projects) || !projects.length)
  {
    message.channel.send("No projects at the moment.");
  }
  else
  {
    let projEmbed = new Discord.MessageEmbed()
    .setColor([200, 200, 200])
    .setTitle('Project List');
    for(i = 0; i < projects.length; i++)
    {
      projEmbed.addField(`**${projects[i].projectname}**`, `${projects[i].description}\nTODO: ${projects[i].todo}`);
    }
    message.channel.send({embed: projEmbed});
  }
}

module.exports = {
    name: 'displayprojects',
    description: 'Displays all registered projects and their reference words.',
    arguments: '[Expand Projects?] (expanded/extended)',
    type: 'Project',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        if(!args.length || (args[0].toLowerCase() != 'expanded' && args[0].toLowerCase() != 'extended'))
        {
            displayProjects(message);
        }
        else
        {
            displayProjectsExpanded(message);
        }
    }
};