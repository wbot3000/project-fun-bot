const db = require("../../database.js");

const {checkForReferenceWord} = require("../../basicFunctions.js");

function updateProjectInfo(message, ref, args)
{
    let field;
    if(args[0] == null || (args[0].toLowerCase() != "reference" && args[0].toLowerCase() != "projectname" && args[0].toLowerCase() != "description" && args[0].toLowerCase() != "todo"))
    {
      message.channel.send("A valid field was not found.");
    }
    else
    {
      field = args.shift().toLowerCase();
      let update;
      if(args == null)
      {
        update = "N/A";
      }
      else
      {
        let upperLimit;
        switch(field) //Determines max number of characters based on field changed
        {
            case("reference"):
            {
                upperLimit = 20;
                break;
            }
            case("projectname"):
            {
                upperLimit = 50;
                break;
            }
            default:
            {
              upperLimit = 200;
            }
        }
        update = args.join(" ");
      }

      if(update.indexOf(" ") != -1 && field == "reference")
      {
        message.channel.send("A project reference has to be one word.");
      }
      else
      {
        db.updateProject(ref, field, update, upperLimit);
        message.channel.send("Project has been updated.");  
      }
    }
}

module.exports = {
    name: 'updateproject',
    description: `Lets you update a project field. Make sure to include a reference word, and what field you'd like to change (reference, projectname, description, todo).`,
    arguments: '<Reference Word>, <Field to Change>, [New Info] (required when changing reference word)',
    type: 'Project',
    admin: true,
    hidden: false,

    execute(message, args)
    {
      checkForReferenceWord(message, args, updateProjectInfo);
    }
};