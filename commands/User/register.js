const db = require("../../database.js");

const {grabDM} = require("../../basicFunctions.js");

const phases = {
    SUBTITLE: 1,
    DESC: 2
}

async function register(message)
{
    message.client.inSetup.push(message.author.id);
    let dmChannel = await grabDM(message.author);
    let userId = message.author.id;
    let user = db.getUser(userId);
    if(!user)
    {
        let subtitle = null
        let profileinfo = null

        dmChannel.send(`Time to get you registered with the bot. First, write yourself a subtitle that'll display on your profile.` +
        ` "What the heck are you talking about?", you ask? Well, here's an example...\n\n\n__John Doe__\n` +
        `*The coolest dude around!**\n\n\n"The coolest dude around!" would be the subtitle. Don't worry, you can change this later.` +
        ` Max length stored is 50 characters.`);

        let filter = m =>
        {
            return m.author.id == userId;
        };
        let collector = dmChannel.createMessageCollector(filter);
        let state = phases.SUBTITLE;

        collector.on("collect", (m) =>
        {
            if(state == phases.SUBTITLE)
            {
                subtitle = m.content;
                state = phases.DESC;
                dmChannel.send(`Now type up a description for yourself. List what you like, your skills, anything ya like. This will also display on your profile.` +
                `Just like the subtitle, this can be updated later. Max length stored is 200 characters.`);
            }
            else if(state == phases.DESC)
            {
                profileinfo = m.content;
                db.insertUser(userId, subtitle, profileinfo);
                dmChannel.send("Profile registration complete.");
                collector.stop();
                message.client.inSetup.splice(message.client.inSetup.indexOf(userId), 1);
            }
        });
    }
    else
    {
        dmChannel.send("You are already registered as a contributor.");
    }
}

module.exports = {
    name: 'register',
    description: 'Registers the user with the bot.',
    arguments: 'None (has a setup process)',
    type: 'User',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        register(message);
    }
};