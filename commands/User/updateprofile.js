const db = require("../../database.js");

function updateProfile(message, [field, ...updateString])
{
    let userId = message.author.id;
    let newInfo = "N/A";
    let upperLimit = 999;

    if(field == undefined || ((field != 'subtitle') && (field != 'profileinfo')))
    {
        message.channel.send('A valid field was not found.');
    }
    else
    {
        if(updateString != undefined)
        {
            newInfo = updateString.join(" ");
            switch(field)
            {
                case('subtitle'):
                {
                    upperLimit = 50;
                    break;
                }
                case('profileinfo'):
                {
                    upperLimit = 200;
                    break;
                }
            }
        }

        db.updateUser(userId, field, newInfo, upperLimit);
    }
}

module.exports = {
    name: 'updateprofile',
    description: 'Lets you update your user profile information. Make sure to specify what you want to update (subtitle, profileinfo)',
    arguments: '<Field> (subtitle/profileinfo), [New Information]',
    type: 'User',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        updateProfile(message, args);
    }
};