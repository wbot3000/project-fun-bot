const db = require("../../database.js");

async function autoregister(message)
{
    let userId = message.author.id;
    let user = db.getUser(userId);
    if(!user)
    {
        db.insertUser(userId);
    }
    else
    {
        message.channel.send("You are already registered as a contributor.");
    }
}

module.exports = {
    name: 'autoregister',
    description: 'Creates you a profile without the setup process.',
    arguments: 'None',
    type: 'User',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        autoregister(message);
    }
};