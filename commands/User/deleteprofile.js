const db = require("../../database.js");

module.exports = {
    name: 'deleteprofile',
    description: 'Unregisters the user from the bot.',
    arguments: 'None',
    type: 'User',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        db.deleteUser(message.author.id);
        message.channel.send('You have been unregistered from the bot.');
    }
};