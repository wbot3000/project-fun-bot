const Discord = require("discord.js");
const db = require("../../database.js");
const roleIDs = require("../../roleids.json");

const {isAdmin} = require("../../basicFunctions.js");
const about = require("../Basic/about.js");

async function showProfile(message, args)
{
    let users = Array.from(message.mentions.users.values());

    if(!Array.isArray(args) || !args.length)
    {
        users = [message.author];
    }

    users.forEach(profileOwner =>
    {
        let profile = db.getUser(profileOwner.id)

        if(!profile)
        {
            if(profileOwner.id == message.author.id)
            {
                message.channel.send("You don't have a profile yet. Use >=register to create a profile, or use >autoregister to skip the setup.");
            }
            else if(profileOwner.id == roleIDs.self)
            {
                about.execute(message, args);
            }
            else
            {
                message.channel.send("This user doesn't have a profile yet. Ask them to >register or >autoregister today!");
            }
        }
        else
        {
            profileUsername = profileOwner.username;
            profileUsername += isAdmin(message, profileOwner) ? `   (⭐)` : '';
            const profileEmbed = new Discord.MessageEmbed()
            .setTitle(profileUsername)
            .setDescription(`**${profile.subtitle}**\n\n${profile.profileinfo}`).setColor([200, 200, 200])
            .setThumbnail(profileOwner.avatarURL());
            message.channel.send({embed:profileEmbed});
        }
    });
}

module.exports = {
    name: 'showprofile',
    description: 'Shows you your profile.',
    arguments: '[User] (mention)',
    type: 'User',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        showProfile(message, args);
    }
};