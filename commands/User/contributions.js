const db = require("../../database.js");

function contributions(message, args)
{
    let contributor = message.author;
    if(Array.isArray(args) && args.length)
    {
        let mentioned = message.mentions.users.first();
        if(mentioned != undefined)
        {
            contributor = mentioned;
        }
    }
    
    let contributionList = db.getProjectsOfUser(contributor.id);

    if(!Array.isArray(contributionList) || !contributionList.length)
    {
        let user = contributor == message.author ? "You" : "This user";
        message.channel.send(user + " didn't register to any projects yet.");
    }
    else
    {
        contributionList.forEach(link => {
            let project = db.getProjectById(link.projectId);
            message.channel.send(project.projectname + ": " + link.note);
        });
    }

}

module.exports = {
    name: "contributions",
    description: "Displays the contributions of a certain user",
    arguments: "[User]",
    type: "Project",
    admin: false,
    hidden: false,

    execute(message, args)
    {
        contributions(message, args);
    }
};