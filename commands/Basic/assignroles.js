const roleIDs = require("../../roleids.json");

async function gatherRoles(message)
{
    let roleList = message.client.serverRef.roles.cache;
    let selfRole = roleList.find(role => role.id === roleIDs.self);
    let bottomRole = roleList.find(role => role.name == roleIDs.voicechatter);
    roleList.each(role => 
        {
            //Can't give administrator roles, the everyone role, roles created by integrations (like bot roles), or roles higher than the bot
            if(!role.permissions.has("ADMINISTRATOR") && !role.managed && selfRole.comparePositionTo(role) > 0 && bottomRole.comparePositionTo(role) < 0)
            {
                try
                {
                    roleToMessage(role, message);
                }
                catch(e)
                {
                    console.error(e);
                }
            }
        }
    );
}

async function roleToMessage(role, message)
{
    try
    {
        let newMsg = await message.channel.send(role.name);
        const filter = (reaction) =>
        {
            return reaction.emoji.name === '👍';
        };

        const collector = newMsg.createReactionCollector(filter);

        collector.on('collect', async (reaction) =>
        {
            try
            {
                //Tries to add the role to the person who just reacted to the message
                let member = reaction.client.serverRef.member(reaction.users.cache.first());
                member.roles.add(role);
                //await reaction.remove();
            }
            catch(e)
            {
                console.error(e);
            }
        });
    }
    catch(e)
    {
        console.error(e);
    }
}

module.exports = {
    name: 'assignroles',
    description: 'Lets a user assign themselves certain roles via message reactions.',
    arguments: 'None',
    type: 'Basic',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        message.channel.send("React to any of these messages with 👍 to give yourself the role. If you don't recieve the role afterwords, call this function again and react to the new set of messages instead.");
        gatherRoles(message);
    }
};