//INCOMPLETE: 

const Discord = require("discord.js");
const fs = require("fs");
const roleIDs = require("../../roleids.json");
const {setCommands} = require("../../basicFunctions");

function reload(message, args)
{
    if(message.author.id != roleIDs.dev)
    {
        message.channel.send("Only the bot owner can reload commands.");
    }
    else
    {
        
        if(!Array.isArray(args) || !args.length) //Reloads all commands
        {
            message.client.commands = new Discord.Collection();
            Object.keys(require.cache).forEach(key => 
            {
                delete require.cache[key];
            }); //Deletes all requires from require.cache

            /*
            const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js') && file != 'basicFunctions.js');

            for (const file of commandFiles) { //Iterates through all files in commandFiles
            const command = require(`./${file}`);
            //console.log(command); //Printer
	        message.client.commands.set(command.name, command);
            }
            */
           /*
            message.client.commandFiles = fs.readdirSync(message.client.commandPathway, {withFileTypes: true});

            for(const category of message.client.commandFiles)
            {
                if(category.isDirectory())
                {
                    let typePath = fs.readdirSync(message.client.commandPathway + `/${category.name}`);
                    for(const file of typePath)
                    {
                        const command = require(message.client.commandPathway + `/${category.name}/${file}`);
                        client.commands.set(command.name, command);
                    }
                }
            }
            */
            setCommands(message.client);
            message.channel.send(`All files have been reloaded.`);
        }
        else //This part doesn't work at the moment
        {
            let removed = message.client.commands.delete(args[0].toLowerCase());
            if(!removed)
            {
                message.channel.send("That command does not exist.");
            }
            else
            {
                delete require.cache[require.resolve(`./${args[0].toLowerCase()}.js`)];
                const newCommandVer = fs.readFileSync(`.CBBot/commands/${args[0].toLowerCase()}.js`);
                console.log(newCommandVer);
                message.client.commands.set(newCommandVer.name, require(`.CBBot/commands/${newCommandVer}`));
                message.channel.send(`${args[0].toLowerCase()}.js has been reloaded`);
            }
        }
    }
}

module.exports = {
    name: `reload`,
    description: `Reloads any functions that are edited while the bot is still active. Only usable by the bot owner.`,
    arguments: `None`,
    type: `Basic`,
    admin: true,
    hidden: true,

    execute(message, args)
    {
        reload(message, args);
    }
};