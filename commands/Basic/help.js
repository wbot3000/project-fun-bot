const Discord = require("discord.js");
const fs = require("fs");
const {grabDM} = require("../../basicFunctions");


async function help(message, args)
{
    //const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js') && file != 'basicFunctions.js'); //Loads all function files into commandFiles
    const dmChannel = await grabDM(message.author);

    if(!Array.isArray(args) || !args.length) //If no arguments are provided
    {
        let projCategories = new Map();

        /*
        message.client.categories.forEach(commandType =>
        {
            if(commandType.isDirectory())
            {
                projCategories.set(commandType.name, []);
            }
        });
        */

        message.client.commands.forEach(command => { //Iterates through all files in commandFiles
            if(!command.hidden)
            {
                //projCategories.get(command.type).push(command);
                if(!projCategories.has(command.type))
                {
                    projCategories.set(command.type, []);
                }
                projCategories.get(command.type).push(command.name)
            }
        });


        dmChannel.send(`**Here's a list of commands (and their command types) for the Project Fun Bot**`);
        projCategories.forEach((commandNameList, projType) => //Goes through projCategories map, making an embed for each 
        {
            let helpEmbed = new Discord.MessageEmbed().setColor([125, 125, 0])
            .setTitle(projType + " Commands");
            let projectList = '';
            commandNameList.forEach(commandName => //Iterates through command array linked with 
            {
                projectList += `\n=>${commandName}`;
            });
            helpEmbed.setDescription(projectList);
            dmChannel.send({embed: helpEmbed}); 
        });
        dmChannel.send(`\nPass a command name to see specifics.`);
    }
    else
    {
        let typeFound = false;
        message.client.commands.forEach(command =>
        { //Iterates through all files in commandFiles
            if(command.name == args[0].toLowerCase())
            {
                let helpEmbed = new Discord.MessageEmbed()
                .setColor([125, 125, 0])
                .setTitle(command.name)
                .setDescription(command.description)
                .addFields(
                    { name: 'Type:', value: command.type},
                    { name: 'Arguments:', value: command.arguments}
                );
                if(command.admin) //If command can only be used by admins
                {
                    helpEmbed.addField('Admins Only', '⭐');
                }
                dmChannel.send({embed: helpEmbed});
                typeFound = true;
            }
        });
        if(!typeFound)
        {
            dmChannel.send("This command could not be found.");
        }
    }

    if(message.channel.type != "dm")
    {
        message.channel.send("DM sent.");
    }
}

module.exports = {
    name: 'help',
    description: 'Helps you with bot controls.',
    arguments: '[Command Name]',
    type: 'Basic',
    admin: false,
    hidden: false,
    
    execute(message, args)
    {
        help(message, args);
    }
};