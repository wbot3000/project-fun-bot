const roleIDs = require("../../roleids.json");

async function grant(message, args)
{
    let requestedRoles = Array.from(message.mentions.roles.values());

    if(!requestedRoles)
    {
        message.channel.send("No roles were specified.");
    }
    else
    {
        let member = message.client.serverRef.member(message.author);
        requestedRoles.forEach(role =>
        {
            if(role.permissions.has("ADMINISTRATOR") || role.managed || role.comparePositionTo(roleIDs.self) > 0 || role.comparePositionTo(roleIDs.voicechatter) < 0)
            {
                message.channel.send(`You can't grant yourself ${role.name}`);
            }
            else
            {
                member.roles.add(role);
                message.channel.send(`You now have the role ${role.name}`);
            }
        });
    }
}

module.exports = {
    name: 'grant',
    description: 'Lets a user assign themselves certain roles via message reactions.',
    arguments: '<Role(s)>',
    type: 'Basic',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        grant(message, args);
    }
};