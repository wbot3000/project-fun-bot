const Discord = require("discord.js");
const fs = require("fs");
const {version, author} = require("../../package.json");


async function about(message, args)
{
    let aboutEmbed = new Discord.MessageEmbed()
    .setColor([125, 125, 0])
    .setTitle("About")
    .setDescription("This is a bot designed for managing projects inside of Discord. Also has some other fun features.")
    .addFields(
        { name: 'Author:', value: author},
        { name: 'Version:', value: version}
    );
    message.channel.send({embed: aboutEmbed});
}

module.exports = {
    name: 'about',
    description: 'Displays one of those "About" summaries.',
    arguments: 'None',
    type: 'Basic',
    admin: false,
    hidden: false,
    
    execute(message, args)
    {
        about(message, args);
    }
};