const fs = require("fs");

async function suggestion(message, args)
{
    if(!Array.isArray(args) || !args.length)
    {
        message.channel.send("You didn't provide a suggestion.");
    }
    else
    {
        let suggestion = args.join(" ") + "\n\n";
        fs.appendFile("UserSuggestions.txt", suggestion, e =>
        {
            if(e)
            {
                console.error(e);
            }
            else
            {
                message.channel.send("Suggestion recorded.");
            }
        });

    }
}

module.exports = {
    name: 'suggestion',
    description: 'Give a suggestion to the bot creator.',
    arguments: '<Suggestion>',
    type: 'Basic',
    admin: false,
    hidden: false,
    
    execute(message, args)
    {
        suggestion(message, args);
    }
};