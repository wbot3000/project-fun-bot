const momSentences = ["Type sentences here to fill in the joke."];

module.exports = {
    name: 'mom',
    description: `Says a "My Mom!" joke, much like Muscle Man from the TV show "Regular Show". Inspired by a friend of mine.`,
    arguments: 'None',
    type: 'Basic',
    admin: false,
    hidden: false,

    execute(message, args)
    {
        let random = Math.floor(Math.random() * momSentences.length); //Picks a random number between 0 to momSentences.length - 1
        message.channel.send(`You know who else ${momSentences[random]}? MY MOM!`);
    }
};