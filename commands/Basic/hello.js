module.exports = {
    name: 'hello',
    description: 'Make the Project Fun Bot say hello to you.',
    arguments: 'None',
    type: 'Basic',
    admin: false,
    hidden: false,
    
    execute(message, args)
    {
        let greeting = `Hello ${message.author.username}, welcome to `;
        if(message.channel.type == 'dm')
        {
            greeting += "our DM channel.";
        }
        else if(message.channel.type == 'text')
        {
            greeting += `${message.guild.name}.`;
        }
        message.channel.send(greeting);
    }

};